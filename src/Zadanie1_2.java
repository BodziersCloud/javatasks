import java.util.Scanner;

public class Zadanie1_2 {

    public static void main(String[] args) {
        System.out.println("ZADANIE 1");
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj liczbę do sprawdzenia: ");
        int zadanie1_Zmienna1 = input.nextInt();

        //int zadanie1_Zmienna1 = 100;
        if (zadanie1_Zmienna1 >= 1 && zadanie1_Zmienna1 <= 10) {
            System.out.println("Liczba " + zadanie1_Zmienna1 + " w przedziale 1-10");
        } else if (zadanie1_Zmienna1 > 10 && zadanie1_Zmienna1 <= 100) {
            System.out.println("Liczba " + zadanie1_Zmienna1 + " w przedziale 11-100");
        } else if (zadanie1_Zmienna1 > 101 && zadanie1_Zmienna1 <= 1001) {
            System.out.println("Liczba " + zadanie1_Zmienna1 + " w przedziale 101-1001");
        } else if (zadanie1_Zmienna1 > 1001) {
            System.out.println("Liczba " + zadanie1_Zmienna1 + " większa od 1001");
        }
    }
}