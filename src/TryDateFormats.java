import java.time.LocalDate;
import java.util.Scanner;

public class TryDateFormats {
    public static void main(String args[]) {
        int day;
        int year;
        int month;
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj rok: ");
        year = input.nextInt();
        System.out.println("Podaj miesiąc: ");
        month = input.nextInt();
        System.out.println("Podaj dzień: ");
        day = input.nextInt();

        LocalDate localDate = LocalDate.of(year, month, day);

        //Getting the day of week for a given date
        java.time.DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        System.out.println(localDate + " to " + dayOfWeek);

        switch (dayOfWeek) {
            case MONDAY:
                System.out.println("Do weekendu jeszcze daleko, 5 dni");break;
            case TUESDAY:
                System.out.println("Do weekendu jeszcze 4 dni, wytrzymasz!");break;
            case WEDNESDAY:
                System.out.println("Połowa tygodnia, już prawie weekend");break;
            case THURSDAY:
                System.out.println("Już mały piątek");break;
            case FRIDAY:
                System.out.println("Piątek, weekendu początek");break;
            default:
                System.out.println("Weekend!");break;
        }
    }

}

