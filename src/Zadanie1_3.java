import java.util.Scanner;
public class Zadanie1_3 {

    public static void main(String[] args) { ;
        System.out.println("ZADANIE 2");
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj ocenę: ");
        int ocena = input.nextInt();

        switch (ocena) {
            case 1:
                System.out.println("niedostateczny");break;
            case 2:
                System.out.println("mierny"); break;
            case 3:
                System.out.println("dostateczny");break;
            case 4:
                System.out.println("dobry");break;
            case 5:
                System.out.println("bardzo dobry");break;
            case 6:
                System.out.println("celujący");break;
            default:
                System.out.println("liczba poza zakresem");
        }
    }
}