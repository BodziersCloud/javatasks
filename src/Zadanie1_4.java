import java.util.Scanner;

public class Zadanie1_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("ZADANIE 3");
        System.out.println("Podaj liczbę do sprawdzenia: ");
        int zadanie1_4_Zmienna1 = input.nextInt();
        if (zadanie1_4_Zmienna1 % 3 == 0 && (zadanie1_4_Zmienna1 % 5 == 0)) {
            System.out.println("Podana liczba podzielna przez 3 i 5");
        } else if (zadanie1_4_Zmienna1 % 5 == 0) {
            System.out.println("Podana liczba podzielna przez 5");
        } else if  (zadanie1_4_Zmienna1 % 3 == 0) {
            System.out.println("Podana liczba podzielna przez 3");
        } else {
            System.out.println("Podana liczba nie jest podzielna przez 3 ani przez 5");
        }

    }
}
