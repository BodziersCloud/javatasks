public class Zadanie1_6 {
    public static void main(String[] args) {
        System.out.println("ZADANIE 6");

        int alphabetNo = 65;
        System.out.println("Pięc pierwszych liter alfabetu łacińskiego to: ");
        for (int i=alphabetNo; i<=alphabetNo+5; i++) {
            System.out.print((char)i + " " );
        }
        System.out.println("");
        alphabetNo = 1488;
        System.out.println("Pięc pierwszych liter alfabetu hebrajskiego to: ");
        for (int i=alphabetNo; i<=alphabetNo+5; i++) {
            System.out.print((char)i + " " );
        }
        System.out.println("");
        alphabetNo = 3840;
        System.out.println("Pięc pierwszych liter alfabetu tybetańskiego to: ");
        for (int i=alphabetNo; i<=alphabetNo+5; i++) {
            System.out.print((char)i + " " );
        }
    }
}
